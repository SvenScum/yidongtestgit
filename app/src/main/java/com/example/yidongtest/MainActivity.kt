package com.example.yidongtest

import android.app.DevInfoManager
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.KeyEvent
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.chinamobile.SWDevInfoManager
import com.chinamobile.authclient.AuthClient
import com.chinamobile.authclient.Constants
import com.lzy.okgo.OkGo
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject


class MainActivity : AppCompatActivity() {
     var cmTokenId : String ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var mAuthClient = AuthClient.getIntance(this)

        var webSettings = webview!!.settings
        webSettings.javaScriptEnabled = true
        //webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.allowFileAccess = true// 设置允许访问文件数据
        webSettings.setSupportZoom(true)//支持缩放
        webSettings.cacheMode = WebSettings.LOAD_NO_CACHE
        webSettings.domStorageEnabled = true
        webSettings.databaseEnabled = true
        webSettings.loadsImagesAutomatically = true
        webSettings.useWideViewPort = true
        webSettings.loadWithOverviewMode = true
        webSettings.setAppCacheEnabled(true)
        webSettings.setGeolocationEnabled(true)

        webview.clearCache(true)

        webview.webChromeClient = WebChromeClient()
        webview!!.setWebViewClient(WebViewClient())

        webview.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                return super.shouldOverrideUrlLoading(view, url)
            }
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                Log.d("AuthClienTest succ", "url+$url")
            }


        });

        mAuthClient.getToken(object :AuthClient.CallBack{
            override fun onResult(p0: JSONObject?) {
                if (p0!!.getInt(Constants.VALUNE_KEY_RESULT_CODE)==Constants.RESULT_OK){
                    cmTokenId = p0!!.getString(Constants.VALUNE_KEY_TOKEN);
                    Log.d("AuthClienTest succ", "aa+${getE()}")
                    handler.sendEmptyMessage(1)
                }else{
                }
            }
        });


    }

     override fun onStart() {
         super.onStart()

        OkGo.get<String>(getE()+"/Ott/jsp/Auth.jsp?UserID=13584047290&OTTUserToken=JSHDC-ASPIRE-6153572b-e354-4d0f-94a8-cd359b1df72e&ContentID=pjsag003470100000000000000007469&ProductCode=sanhecbyc&Name=云唱唱吧&Type=0&BizType=1&Price=2500&ValidStartTime=202009211552&ValidEndTime=202010211552")
     }

        fun getE(): String {
            val mDevInfoManager: DevInfoManager = SWDevInfoManager.getDevInfoManager(this)
            var phoneNumber = ""
            var Account = ""
            var STBMAC = ""
            var STBSN = ""
            var PlatformURL = ""
            var PlatformURLBackup = ""
            var CDNType = ""
            var EPGAddress = ""
            if (mDevInfoManager != null) {
//sss
                phoneNumber = mDevInfoManager.getValue(DevInfoManager.PHONE)
                Account = mDevInfoManager.getValue(DevInfoManager.ACCOUNT)
                STBMAC = mDevInfoManager.getValue(DevInfoManager.STB_MAC)
                STBSN = mDevInfoManager.getValue(DevInfoManager.STB_SN)
                PlatformURL = mDevInfoManager.getValue(DevInfoManager.PLAT_URL)
                PlatformURLBackup = mDevInfoManager.getValue(DevInfoManager.PLAT_URLBACK)
                CDNType = mDevInfoManager.getValue(DevInfoManager.CDN_TYPE)
                EPGAddress = mDevInfoManager.getValue(DevInfoManager.EPG_ADDRESS) //EPG_ADDRESS
            }
            val sb = StringBuffer()
            sb.append(EPGAddress)
            /*sb.append("Account=$Account&")
            sb.append("STBMAC=$STBMAC&")
            sb.append("STBSN=$STBSN&")
            sb.append("PlatformURL=$PlatformURL&")
            sb.append("PlatformURLBackup=$PlatformURLBackup&")*/
            return sb.toString();
        }


     val handler : Handler = object : Handler() {
         override fun handleMessage(msg: Message?) {
             super.handleMessage(msg)
             when (msg?.what) {
                 1 -> {

                     Log.d("AuthClienTest succ", "http://gdsc.cmshop.net/yigou/zazhi/changba_jiangsu_yidong/jump_rukou.html?cmtoken=$cmTokenId")
                     webview.loadUrl("http://gdsc.cmshop.net/yigou/zazhi/changba_jiangsu_yidong/jump_rukou.html?cmtoken=$cmTokenId")
                     //webview.loadUrl("http://gdsc.cmshop.net/yigou/zazhi/changba_jiangsu_yidong/index.html")


                 }
             }
         }
     }
     var exitTime: Long = 0
     override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
         Log.d("AuthClienTest succ", webview.url)
         if (keyCode == KeyEvent.KEYCODE_BACK && webview.url.contains("http://gdsc.cmshop.net/yigou/zazhi/changba_jiangsu_yidong/index.html")){

                 finish();

             return true;
         }

         if (keyCode == KeyEvent.KEYCODE_BACK && webview.url.contains("http://gdsc.cmshop.net/yigou/zazhi/changba_jiangsu_yidong/ndex_diange.html")){

             finish();

             //cc
             return true;
         }else if (webview.url.contains("http://gdsc.cmshop.net/yigou/zazhi/changba_jiangsu_yidong/video_player/video/new_video.html")){
             startActivity(Intent(this,PlayActivity::class.java))
             webview.goBack()
         }else{
             webview.goBack()
             return true;
         }
         //111
         return super.onKeyDown(keyCode, event)
     }
}
