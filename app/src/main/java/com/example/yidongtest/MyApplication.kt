package com.example.yidongtest

import android.app.Application
import com.lzy.okgo.OkGo
import com.lzy.okgo.cache.CacheEntity
import com.lzy.okgo.cache.CacheMode
import com.lzy.okgo.cookie.CookieJarImpl
import com.lzy.okgo.cookie.store.DBCookieStore
import com.lzy.okgo.interceptor.HttpLoggingInterceptor
import com.lzy.okgo.model.HttpHeaders
import com.lzy.okgo.model.HttpParams
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit
import java.util.logging.Level


class MyApplication :Application(){

    override fun onCreate() {
        super.onCreate()
        //okGo网络框架初始化和全局配置
        //okGo网络框架初始化和全局配置
        val builder = OkHttpClient.Builder()
        //builder.cookieJar(new CookieJarImpl(new SPCookieStore(this)));      //使用sp保持cookie，如果cookie不过期，则一直有效
        //builder.cookieJar(new CookieJarImpl(new SPCookieStore(this)));      //使用sp保持cookie，如果cookie不过期，则一直有效
        builder.cookieJar(CookieJarImpl(DBCookieStore(this))) //使用数据库保持cookie，如果cookie不过期，则一直有效

        //builder.cookieJar(new CookieJarImpl(new MemoryCookieStore()));      //使用内存保持cookie，app退出后，cookie消失
        //设置请求头
        //builder.cookieJar(new CookieJarImpl(new MemoryCookieStore()));      //使用内存保持cookie，app退出后，cookie消失
//设置请求头
        val headers = HttpHeaders()
        headers.put("commonHeaderKey1", "commonHeaderValue1") //header不支持中文，不允许有特殊字符

        headers.put("commonHeaderKey2", "commonHeaderValue2")
        //设置请求参数
        //设置请求参数
        val params = HttpParams()
        params.put("commonParamsKey1", "commonParamsValue1") //param支持中文,直接传,不要自己编码

        params.put("commonParamsKey2", "这里支持中文参数")
        OkGo.getInstance().init(this) //必须调用初始化
            .setOkHttpClient(builder.build()) //建议设置OkHttpClient，不设置会使用默认的
            .setCacheMode(CacheMode.NO_CACHE) //全局统一缓存模式，默认不使用缓存，可以不传
            .setCacheTime(CacheEntity.CACHE_NEVER_EXPIRE) //全局统一缓存时间，默认永不过期，可以不传
            .setRetryCount(3) //全局统一超时重连次数，默认为三次，那么最差的情况会请求4次(一次原始请求，三次重连请求)，不需要可以设置为0
            .addCommonHeaders(headers) //全局公共头
            .addCommonParams(params)
    }


    /**
     * 初始化OKgo的环境配置【可选】
     */
    private fun initOkHttp() { //使用OkGo内置的log拦截器打印log，如果你觉得不好用，也可以自己写个，这个没有限制。
        val interceptor = HttpLoggingInterceptor("okgo")
        //log打印级别，决定了log显示的详细程度
        interceptor.setPrintLevel(HttpLoggingInterceptor.Level.BODY)
        //log颜色级别，决定了log在控制台显示的颜色
        interceptor.setColorLevel(Level.INFO)
        val builder = OkHttpClient.Builder()
        //全局的读取超时时间 十分钟
        builder.readTimeout(60 * 1000 * 10.toLong(), TimeUnit.MILLISECONDS)
        //全局的写入超时时间 十分钟
        builder.writeTimeout(60 * 1000 * 10.toLong(), TimeUnit.MILLISECONDS)
        //全局的连接超时时间 十分钟
        builder.connectTimeout(60 * 1000 * 10.toLong(), TimeUnit.MILLISECONDS)
        builder.addInterceptor(interceptor)
        val client = builder.build()
        OkGo.getInstance().init(this).okHttpClient = client

    }
}